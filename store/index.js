import Vuex from "vuex";

import user from "./modules/user";
import cart from "./modules/cart";
const store = () => {
  return new Vuex.Store({
    state: {
      value: 0
    },
    modules: {
      user,
      cart
    }
  });
};
export default store;
