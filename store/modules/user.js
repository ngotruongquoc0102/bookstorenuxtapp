import * as types from "../types";
const cookieparser = require("cookieparser");
const cookie = require("js-cookie");

const getUserFromLocalStorage = () => {
  let user = null;
  try {
    user = JSON.parse(localStorage.getItem("user"));
    return user;
  } catch (error) {}
};
const state = {
  user: getUserFromLocalStorage()
};
// Properties getters
const getters = {
  [types.GET_USER]: state => {
    return state.user;
  }
};
//Synchronus Function
const mutations = {
  [types.MUTATE_SET_USER]: (state, payload) => {
    state.user = payload;
  },
  [types.MUTATE_CHECK_USER]: (state, payload) => {
    //return state.user;
    console.log("here", state.user);
  }
};
//Asynchronous Funtion
const actions = {
  nuxtServerInit({ commit }, { req }) {
    console.log("server init");
    let auth = null;
    if (req.headers.cookie) {
      const parser = cookieparser.parse(req.headers.cookie);
      try {
        auth = JSON.parse(parser.auth);
      } catch (error) {}
    }
    commit("user/MUTATE_SET_USER", auth);
  },
  [types.ON_SUBMIT_LOGIN_ASYNC]: async ({ commit }, payload) => {
    console.log("payload", payload);
    await cookie.set("auth", payload, { expires: 7 });
    commit("user/MUTATE_SET_USER", payload);
  },
  [types.ACTION_LOGOUT]: async ({ commit }, payload) => {
    await cookie.remove("auth");
    commit("user/MUTATE_SET_USER", null);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
