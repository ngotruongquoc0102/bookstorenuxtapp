import * as types from "../types";
import { type } from "jquery";

const state = {
  cartProducts: [],
  cartCheckout: []
};
const getters = {
  [types.GET_PRODUCT_IN_CART]: state => {
    return state.cartProducts;
  },
  [types.GET_PRODUCT_CHECKOUT_IN_CART]: state => {
    return state.cartCheckout;
  }
};

const mutations = {
  [types.MUTATE_ADD_PRODUCT]: (state, payload) => {
    state.cartProducts.push(payload);
    localStorage.setItem("listProduct", JSON.stringify(state.cartProducts));
  },
  [types.MUTATE_SET_PRODUCT]: (state, payload) => {
    state.cartProducts = payload;
  },
  [types.MUTATE_REMOVE_PRODUCT]: (state, payload) => {
    state.cartProducts.splice(payload, 1);
    localStorage.setItem("listProduct", JSON.stringify(state.cartProducts));
  },
  [types.MUTATE_SET_PRODUCT_CHECKOUT]: (state, payload) => {
    state.cartCheckout = payload;
    console.log("checkout", state.cartCheckout);
  },
  [types.MUTATE_GET_PRODUCT_CHECKOUT]: (state, payload) => {},
  [types.MUTATE_SAVE_QUANTITY]: (state, payload) => {
    console.log("save");
    state.cartProducts.forEach(product => {
      if (product.productId === payload.id) {
        product.quantity = payload.quantity;
        console.log("state", state.cartProducts);
        localStorage.setItem("listProduct", JSON.stringify(state.cartProducts));
        return;
      }
    });
  }
};
async function getProductFromLocalStorage() {
  let listProduct = null;
  try {
    if (process.browser) {
      listProduct = await JSON.parse(localStorage.getItem("listProduct"));
      console.log("listhere", listProduct);
      return listProduct;
    }
  } catch (error) {
    console.log("err", error);
    if (process.browser) {
      localStorage.setItem("listProduct", null);
      return [];
    }
  }
}

async function getProductCheckoutFromLocalStorage() {
  let listProduct = null;
  try {
    if (process.browser) {
      listProduct = await JSON.parse(
        localStorage.getItem("listProductCheckout")
      );
      return listProduct;
    }
  } catch (error) {
    console.log("err", error);
    if (process.browser) {
      localStorage.setItem("listProductCheckout", null);
      return [];
    }
  }
}
const actions = {
  [types.ACTION_ADD_PRODUCT]: ({ commit }, payload) => {
    commit(types.MUTATE_ADD_PRODUCT, payload);
  },
  [types.ACTION_SET_PRODUCT]: async ({ commit }, payload) => {
    const data = await getProductFromLocalStorage();
    commit("cart/MUTATE_SET_PRODUCT", data);
  },
  [types.ACTION_REMOVE_PRODUCT]: async ({ commit }, index) => {
    commit(types.MUTATE_REMOVE_PRODUCT, index);
  },
  [types.ACTION_SET_PRODUCT_CHECKOUT]: async ({ commit }, payload) => {
    const map = [];
    await localStorage.setItem("listProductCheckout", JSON.stringify(payload));
  },
  [types.ACTION_GET_PRODUCT_CHECKOUT]: async ({ commit }, payload) => {
    console.log("action");
    const data = await getProductCheckoutFromLocalStorage();
    commit(types.MUTATE_SET_PRODUCT_CHECKOUT, data);
    return data;
  },
  [types.ACTION_SAVE_QUANTITY]: async ({ commit }, payload) => {
    commit(types.MUTATE_SAVE_QUANTITY, payload);
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
