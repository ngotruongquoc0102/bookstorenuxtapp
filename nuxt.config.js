const nodeExternals = require("webpack-node-externals");
export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */

  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */

  // target: "server",
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */

  env: {
    baseUrl: process.env.BASE_URL || "http://localhost:3000",
    STRIPE_PUBLIC_KEY: process.env.STRIPE_PUBLIC_KEY
  },
  server: {
    port: 8000, // default: 3000
    host: "localhost" // default: localhost
  },
  devtools: true,
  head: {
    title: process.env.npm_package_name || "",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: process.env.npm_package_description || ""
      }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Roboto:wght@300;400&display=swap"
      },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.0.6/css/all.css"
      }
    ]
  },
  /*
   ** Global CSS
   */
  css: [
    "@/assets/scss/main.scss",
    "vue-slick-carousel/dist/vue-slick-carousel.css",
    { src: "ant-design-vue/dist/antd.css" },
    { src: "primevue/resources/primevue.min.css" },
    { src: "primevue/resources/themes/saga-blue/theme.css" },
    { src: "primeicons/primeicons.css" }
  ],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    "~/plugins/axios",
    "~/plugins/vue-slick-carousel",
    { src: "~plugins/Vuelidate", ssr: true },
    { src: "~plugins/ckeditor" },
    { src: "~plugins/vuetag", ssr: false },
    { src: "~plugins/sweetAlert", ssr: false },
    "~plugins/ant",
    { src: "~/plugins/primevue.js", mode: "client" },
    { src: "~/plugins/star-rating.js", ssr: false }
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: ["@nuxtjs/dotenv"],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://bootstrap-vue.js.org
    "bootstrap-vue/nuxt",
    "@nuxtjs/axios",
    [
      "nuxt-stripe-module",
      {
        publishableKey:
          "pk_test_51HHVe9HMnSlpqS7rUS9vJKlr1RKHXFOBEUGnkw7meruQVX3KN833rRs6DCfkRAnHA9HWv6m1SFlFvQSJKkz791rC00zvKk6GWV"
      }
    ]
  ],
  stripe: {
    publishableKey:
      "pk_test_51HHVe9HMnSlpqS7rUS9vJKlr1RKHXFOBEUGnkw7meruQVX3KN833rRs6DCfkRAnHA9HWv6m1SFlFvQSJKkz791rC00zvKk6GWV"
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    extend(config, ctx) {
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            allowlist: [/^vue-slick/]
          })
        ];
      }
    }
  }
};
