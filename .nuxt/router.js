import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _9b0c336c = () => interopDefault(import('../pages/404.vue' /* webpackChunkName: "pages/404" */))
const _00d150c2 = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _66c2d302 = () => interopDefault(import('../pages/account/index.vue' /* webpackChunkName: "pages/account/index" */))
const _2769ffb8 = () => interopDefault(import('../pages/admin/index.vue' /* webpackChunkName: "pages/admin/index" */))
const _c90a52fe = () => interopDefault(import('../pages/cart/index.vue' /* webpackChunkName: "pages/cart/index" */))
const _778eb0c8 = () => interopDefault(import('../pages/crud.vue' /* webpackChunkName: "pages/crud" */))
const _2e12550a = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _f53be978 = () => interopDefault(import('../pages/product/index.vue' /* webpackChunkName: "pages/product/index" */))
const _0aa60d81 = () => interopDefault(import('../pages/register.vue' /* webpackChunkName: "pages/register" */))
const _36a4fb00 = () => interopDefault(import('../pages/whishlist/index.vue' /* webpackChunkName: "pages/whishlist/index" */))
const _f7be1bc2 = () => interopDefault(import('../pages/admin/category/index.vue' /* webpackChunkName: "pages/admin/category/index" */))
const _1d1161ba = () => interopDefault(import('../pages/admin/order/index.vue' /* webpackChunkName: "pages/admin/order/index" */))
const _27941904 = () => interopDefault(import('../pages/admin/product/index.vue' /* webpackChunkName: "pages/admin/product/index" */))
const _597fdeef = () => interopDefault(import('../pages/admin/tag/index.vue' /* webpackChunkName: "pages/admin/tag/index" */))
const _350d10a8 = () => interopDefault(import('../pages/admin/user/index.vue' /* webpackChunkName: "pages/admin/user/index" */))
const _6dc64642 = () => interopDefault(import('../pages/book/NewRelease.vue' /* webpackChunkName: "pages/book/NewRelease" */))
const _8a7b95ee = () => interopDefault(import('../pages/checkout/address.vue' /* webpackChunkName: "pages/checkout/address" */))
const _8dec78a2 = () => interopDefault(import('../pages/checkout/delivery.vue' /* webpackChunkName: "pages/checkout/delivery" */))
const _9ded1bca = () => interopDefault(import('../pages/checkout/payment.vue' /* webpackChunkName: "pages/checkout/payment" */))
const _55655a52 = () => interopDefault(import('../pages/components/BaseNav.vue' /* webpackChunkName: "pages/components/BaseNav" */))
const _0484da65 = () => interopDefault(import('../pages/components/BookDetail.vue' /* webpackChunkName: "pages/components/BookDetail" */))
const _6fd917ea = () => interopDefault(import('../pages/components/cart.vue' /* webpackChunkName: "pages/components/cart" */))
const _0b1561b1 = () => interopDefault(import('../pages/components/checkout.vue' /* webpackChunkName: "pages/components/checkout" */))
const _23ad8102 = () => interopDefault(import('../pages/components/Comments.vue' /* webpackChunkName: "pages/components/Comments" */))
const _64586184 = () => interopDefault(import('../pages/components/Content.vue' /* webpackChunkName: "pages/components/Content" */))
const _7f8e75d2 = () => interopDefault(import('../pages/components/crud-category.vue' /* webpackChunkName: "pages/components/crud-category" */))
const _021c96bf = () => interopDefault(import('../pages/components/crud-inactive.vue' /* webpackChunkName: "pages/components/crud-inactive" */))
const _abdad1ac = () => interopDefault(import('../pages/components/crud-order.vue' /* webpackChunkName: "pages/components/crud-order" */))
const _233c6b31 = () => interopDefault(import('../pages/components/crud-owncate.vue' /* webpackChunkName: "pages/components/crud-owncate" */))
const _4ae30baa = () => interopDefault(import('../pages/components/crud-table.vue' /* webpackChunkName: "pages/components/crud-table" */))
const _82e0751c = () => interopDefault(import('../pages/components/newRelease.vue' /* webpackChunkName: "pages/components/newRelease" */))
const _28b394d4 = () => interopDefault(import('../pages/components/product.vue' /* webpackChunkName: "pages/components/product" */))
const _3a2156fc = () => interopDefault(import('../pages/components/SingleComment.vue' /* webpackChunkName: "pages/components/SingleComment" */))
const _7c8bc47c = () => interopDefault(import('../pages/components/tag-crud.vue' /* webpackChunkName: "pages/components/tag-crud" */))
const _24c678d0 = () => interopDefault(import('../pages/components/whishlist.vue' /* webpackChunkName: "pages/components/whishlist" */))
const _12b2cd64 = () => interopDefault(import('../pages/customer/address/index.vue' /* webpackChunkName: "pages/customer/address/index" */))
const _4e93a7fa = () => interopDefault(import('../pages/product/create.vue' /* webpackChunkName: "pages/product/create" */))
const _dcbb703a = () => interopDefault(import('../pages/product/manage.vue' /* webpackChunkName: "pages/product/manage" */))
const _36772bae = () => interopDefault(import('../pages/admin/category/add.vue' /* webpackChunkName: "pages/admin/category/add" */))
const _3c076813 = () => interopDefault(import('../pages/admin/category/own.vue' /* webpackChunkName: "pages/admin/category/own" */))
const _0a33ccf6 = () => interopDefault(import('../pages/admin/category/trash.vue' /* webpackChunkName: "pages/admin/category/trash" */))
const _1ab95b8c = () => interopDefault(import('../pages/admin/product/create.vue' /* webpackChunkName: "pages/admin/product/create" */))
const _039f6ca2 = () => interopDefault(import('../pages/admin/tag/create.vue' /* webpackChunkName: "pages/admin/tag/create" */))
const _1d2c6edc = () => interopDefault(import('../pages/admin/user/create.vue' /* webpackChunkName: "pages/admin/user/create" */))
const _850b25fe = () => interopDefault(import('../pages/admin/user/inactive.vue' /* webpackChunkName: "pages/admin/user/inactive" */))
const _a1bdca7a = () => interopDefault(import('../pages/admin/user/login.vue' /* webpackChunkName: "pages/admin/user/login" */))
const _66724c83 = () => interopDefault(import('../pages/admin/user/profile.vue' /* webpackChunkName: "pages/admin/user/profile" */))
const _6b5b29da = () => interopDefault(import('../pages/customer/address/create.vue' /* webpackChunkName: "pages/customer/address/create" */))
const _c076836a = () => interopDefault(import('../pages/admin/order/_id.vue' /* webpackChunkName: "pages/admin/order/_id" */))
const _25dd77bc = () => interopDefault(import('../pages/admin/product/_slug.vue' /* webpackChunkName: "pages/admin/product/_slug" */))
const _169cd8cc = () => interopDefault(import('../pages/customer/address/_id.vue' /* webpackChunkName: "pages/customer/address/_id" */))
const _6ff18dc2 = () => interopDefault(import('../pages/book/_slug.vue' /* webpackChunkName: "pages/book/_slug" */))
const _40b05e7c = () => interopDefault(import('../pages/category/_slug/_id.vue' /* webpackChunkName: "pages/category/_slug/_id" */))
const _1f4f3264 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _2132f0c7 = () => interopDefault(import('../pages/_slug/_id.vue' /* webpackChunkName: "pages/_slug/_id" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/404",
    component: _9b0c336c,
    name: "404"
  }, {
    path: "/about",
    component: _00d150c2,
    name: "about"
  }, {
    path: "/account",
    component: _66c2d302,
    name: "account"
  }, {
    path: "/admin",
    component: _2769ffb8,
    name: "admin"
  }, {
    path: "/cart",
    component: _c90a52fe,
    name: "cart"
  }, {
    path: "/crud",
    component: _778eb0c8,
    name: "crud"
  }, {
    path: "/login",
    component: _2e12550a,
    name: "login"
  }, {
    path: "/product",
    component: _f53be978,
    name: "product"
  }, {
    path: "/register",
    component: _0aa60d81,
    name: "register"
  }, {
    path: "/whishlist",
    component: _36a4fb00,
    name: "whishlist"
  }, {
    path: "/admin/category",
    component: _f7be1bc2,
    name: "admin-category"
  }, {
    path: "/admin/order",
    component: _1d1161ba,
    name: "admin-order"
  }, {
    path: "/admin/product",
    component: _27941904,
    name: "admin-product"
  }, {
    path: "/admin/tag",
    component: _597fdeef,
    name: "admin-tag"
  }, {
    path: "/admin/user",
    component: _350d10a8,
    name: "admin-user"
  }, {
    path: "/book/NewRelease",
    component: _6dc64642,
    name: "book-NewRelease"
  }, {
    path: "/checkout/address",
    component: _8a7b95ee,
    name: "checkout-address"
  }, {
    path: "/checkout/delivery",
    component: _8dec78a2,
    name: "checkout-delivery"
  }, {
    path: "/checkout/payment",
    component: _9ded1bca,
    name: "checkout-payment"
  }, {
    path: "/components/BaseNav",
    component: _55655a52,
    name: "components-BaseNav"
  }, {
    path: "/components/BookDetail",
    component: _0484da65,
    name: "components-BookDetail"
  }, {
    path: "/components/cart",
    component: _6fd917ea,
    name: "components-cart"
  }, {
    path: "/components/checkout",
    component: _0b1561b1,
    name: "components-checkout"
  }, {
    path: "/components/Comments",
    component: _23ad8102,
    name: "components-Comments"
  }, {
    path: "/components/Content",
    component: _64586184,
    name: "components-Content"
  }, {
    path: "/components/crud-category",
    component: _7f8e75d2,
    name: "components-crud-category"
  }, {
    path: "/components/crud-inactive",
    component: _021c96bf,
    name: "components-crud-inactive"
  }, {
    path: "/components/crud-order",
    component: _abdad1ac,
    name: "components-crud-order"
  }, {
    path: "/components/crud-owncate",
    component: _233c6b31,
    name: "components-crud-owncate"
  }, {
    path: "/components/crud-table",
    component: _4ae30baa,
    name: "components-crud-table"
  }, {
    path: "/components/newRelease",
    component: _82e0751c,
    name: "components-newRelease"
  }, {
    path: "/components/product",
    component: _28b394d4,
    name: "components-product"
  }, {
    path: "/components/SingleComment",
    component: _3a2156fc,
    name: "components-SingleComment"
  }, {
    path: "/components/tag-crud",
    component: _7c8bc47c,
    name: "components-tag-crud"
  }, {
    path: "/components/whishlist",
    component: _24c678d0,
    name: "components-whishlist"
  }, {
    path: "/customer/address",
    component: _12b2cd64,
    name: "customer-address"
  }, {
    path: "/product/create",
    component: _4e93a7fa,
    name: "product-create"
  }, {
    path: "/product/manage",
    component: _dcbb703a,
    name: "product-manage"
  }, {
    path: "/admin/category/add",
    component: _36772bae,
    name: "admin-category-add"
  }, {
    path: "/admin/category/own",
    component: _3c076813,
    name: "admin-category-own"
  }, {
    path: "/admin/category/trash",
    component: _0a33ccf6,
    name: "admin-category-trash"
  }, {
    path: "/admin/product/create",
    component: _1ab95b8c,
    name: "admin-product-create"
  }, {
    path: "/admin/tag/create",
    component: _039f6ca2,
    name: "admin-tag-create"
  }, {
    path: "/admin/user/create",
    component: _1d2c6edc,
    name: "admin-user-create"
  }, {
    path: "/admin/user/inactive",
    component: _850b25fe,
    name: "admin-user-inactive"
  }, {
    path: "/admin/user/login",
    component: _a1bdca7a,
    name: "admin-user-login"
  }, {
    path: "/admin/user/profile",
    component: _66724c83,
    name: "admin-user-profile"
  }, {
    path: "/customer/address/create",
    component: _6b5b29da,
    name: "customer-address-create"
  }, {
    path: "/admin/order/:id",
    component: _c076836a,
    name: "admin-order-id"
  }, {
    path: "/admin/product/:slug",
    component: _25dd77bc,
    name: "admin-product-slug"
  }, {
    path: "/customer/address/:id",
    component: _169cd8cc,
    name: "customer-address-id"
  }, {
    path: "/book/:slug?",
    component: _6ff18dc2,
    name: "book-slug"
  }, {
    path: "/category/:slug?/:id?",
    component: _40b05e7c,
    name: "category-slug-id"
  }, {
    path: "/",
    component: _1f4f3264,
    name: "index"
  }, {
    path: "/:slug/:id?",
    component: _2132f0c7,
    name: "slug-id"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
