export { default as BaseInput } from '../../components/BaseInput.vue'
export { default as Logo } from '../../components/Logo.vue'
export { default as SideBar } from '../../components/SideBar.vue'
export { default as SidebarItem } from '../../components/SidebarItem.vue'
export { default as Table } from '../../components/Table.vue'
export { default as Login } from '../../components/common/login.vue'
export { default as Register } from '../../components/common/register.vue'
export { default as Footer } from '../../components/common/Layout/Footer.vue'
export { default as Header } from '../../components/common/Layout/Header.vue'
export { default as Navbar } from '../../components/common/Layout/Navbar.vue'
export { default as Sidebar } from '../../components/common/Layout/Sidebar.vue'
export { default as BaseHeader } from '../../components/v-layout/BaseHeader/index.vue'
export { default as BaseFooter } from '../../components/v-layout/BaseFooter/index.vue'
export { default as Cart } from '../../components/v-layout/cart/index.vue'
export { default as Content } from '../../components/v-layout/Content/index.vue'

export const LazyBaseInput = import('../../components/BaseInput.vue' /* webpackChunkName: "components/BaseInput'}" */).then(c => c.default || c)
export const LazyLogo = import('../../components/Logo.vue' /* webpackChunkName: "components/Logo'}" */).then(c => c.default || c)
export const LazySideBar = import('../../components/SideBar.vue' /* webpackChunkName: "components/SideBar'}" */).then(c => c.default || c)
export const LazySidebarItem = import('../../components/SidebarItem.vue' /* webpackChunkName: "components/SidebarItem'}" */).then(c => c.default || c)
export const LazyTable = import('../../components/Table.vue' /* webpackChunkName: "components/Table'}" */).then(c => c.default || c)
export const LazyLogin = import('../../components/common/login.vue' /* webpackChunkName: "components/common/login'}" */).then(c => c.default || c)
export const LazyRegister = import('../../components/common/register.vue' /* webpackChunkName: "components/common/register'}" */).then(c => c.default || c)
export const LazyFooter = import('../../components/common/Layout/Footer.vue' /* webpackChunkName: "components/common/Layout/Footer'}" */).then(c => c.default || c)
export const LazyHeader = import('../../components/common/Layout/Header.vue' /* webpackChunkName: "components/common/Layout/Header'}" */).then(c => c.default || c)
export const LazyNavbar = import('../../components/common/Layout/Navbar.vue' /* webpackChunkName: "components/common/Layout/Navbar'}" */).then(c => c.default || c)
export const LazySidebar = import('../../components/common/Layout/Sidebar.vue' /* webpackChunkName: "components/common/Layout/Sidebar'}" */).then(c => c.default || c)
export const LazyBaseHeader = import('../../components/v-layout/BaseHeader/index.vue' /* webpackChunkName: "components/v-layout/BaseHeader/index'}" */).then(c => c.default || c)
export const LazyBaseFooter = import('../../components/v-layout/BaseFooter/index.vue' /* webpackChunkName: "components/v-layout/BaseFooter/index'}" */).then(c => c.default || c)
export const LazyCart = import('../../components/v-layout/cart/index.vue' /* webpackChunkName: "components/v-layout/cart/index'}" */).then(c => c.default || c)
export const LazyContent = import('../../components/v-layout/Content/index.vue' /* webpackChunkName: "components/v-layout/Content/index'}" */).then(c => c.default || c)
