const middleware = {}

middleware['auth'] = require('../middleware/auth.js')
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['authenticated'] = require('../middleware/authenticated.js')
middleware['authenticated'] = middleware['authenticated'].default || middleware['authenticated']

middleware['CheckAdminGranted'] = require('../middleware/CheckAdminGranted.js')
middleware['CheckAdminGranted'] = middleware['CheckAdminGranted'].default || middleware['CheckAdminGranted']

export default middleware
