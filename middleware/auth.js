export default ({ store, error, redirect }) => {
  if (!store.state.user.user) {
    return redirect("/login");
  }
};
