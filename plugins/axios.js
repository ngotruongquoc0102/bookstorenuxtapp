// import axios from 'axios'

// export default axios.create({
//     baseURL: process.env.baseURL
// })
export default function({ $axios, redirect, store }) {
  $axios.defaults.baseURL = `${process.env.SERVER_URL}:${process.env.SERVER_PORT}`;
}
